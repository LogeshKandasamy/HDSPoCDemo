package com.hds.service;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.UpdateItemSpec;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.amazonaws.services.dynamodbv2.model.CreateTableRequest;
import com.amazonaws.services.dynamodbv2.model.ProvisionedThroughput;
import com.hds.model.ContactDetails;
import com.hds.model.Userdetails;

@Service
public class UserService {

	@Autowired(required = true)

	DynamoDB dynamodb;

	@Autowired(required = true)

	DynamoDBMapper dbmapper;

	Table table;
	Userdetails user = null;

	public void createDynamoTable() {

		System.out.println("table creation url hitted");
		try {

			CreateTableRequest tableRequest = dbmapper.generateCreateTableRequest(Userdetails.class);

			tableRequest.setProvisionedThroughput(new ProvisionedThroughput(1L, 1L)); // 2

			table = dynamodb.createTable(tableRequest); // 3

			table.waitForActive(); // 4

			System.out.println("table has been created");

		} catch (Exception e) {
			System.out.println("Exception while handle create request" + e.getMessage());
		}

	}

	// public String add( Userdetails user) throws JSONException
	// {
	// user=new Userdetails();
	// user.setUserid("az12");
	// user.setUsername("reena");
	// user.setPolicyid(12345);
	//
	// JSONObject item=new JSONObject();
	// item.put("claim1", "csectiom");
	// item.put("claim2", "hearttransplantation");
	// item.put("claim2", "generalsurgery");
	// List <JSONObject> claims=new ArrayList<JSONObject>();
	// claims.add(item);
	// String claimslist=claims.toString();
	// user.setClaimslist(claimslist);
	// dbmapper.save(user);
	// return " useradded sucessfully";
	// }

	public void addUser() {
		List<String> claimslist = new ArrayList<String>();
		claimslist.add("csectiom");
		claimslist.add("hearttransplantation");
		claimslist.add("generalsurgery");
		dbmapper.save(new Userdetails("jonny", "st1234", 6723, claimslist,
				new ContactDetails(989880889, "chennai", "contact@gmail.com")));
	}

	public Userdetails get(String userid) {

		// Book bookRetrieved = mapper.load(Book.class, 502);
		// System.out.println("Book info: " + "\n" + bookRetrieved);
		Userdetails user = dbmapper.load(Userdetails.class, userid);
		System.out.println("user info: " + "\n" + user);
		return user;
	}

	public Userdetails updateUserInfo(String id) {

		// Book bookRetrieved = mapper.load(Book.class, 502);
		// System.out.println("Book info: " + "\n" + bookRetrieved);
		//
		// bookRetrieved.getDimensions().setHeight("9.0");
		// bookRetrieved.getDimensions().setLength("12.0");
		// bookRetrieved.getDimensions().setThickness("2.0");
		//
		// mapper.save(bookRetrieved);
		//
		// bookRetrieved = mapper.load(Book.class, 502);
		// System.out.println("Updated book info: " + "\n" + bookRetrieved);
		// return user;
		// update the particular item
		System.out.println("before the table to be updated");
		// String tablename="Userdetails";
		table = dynamodb.getTable("Userdetails");
		String tb = table.getTableName();
		System.out.println(tb);
		table.updateItem(new UpdateItemSpec().withPrimaryKey("userid", id)
				.withUpdateExpression("SET contactDetails.address = :city")
				.withValueMap(new ValueMap().withString(":city", "thanjavur")));

		Userdetails user = dbmapper.load(Userdetails.class, id);
		System.out.println("user info: " + "\n" + user);
		return user;
	}

}
