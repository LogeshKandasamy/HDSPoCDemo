package com.hds.config;

import org.socialsignin.spring.data.dynamodb.repository.config.EnableDynamoDBRepositories;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.amazonaws.AmazonWebServiceClient;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.config.AmazonConfigClient;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;

@Configuration
@EnableDynamoDBRepositories(basePackages = "com.hds.*")
public class AWSApplicationConfig {

	@Value("${amazon.aws.accesskey}")
	private String amazonAWSAccessKey;

	@Value("${amazon.aws.secretkey}")
	private String amazonAWSSecretKey;

	@Value("${amazon.dynamodb.region}")
	private String dbRegion;

	// passing the credentials to the AWSdyanmodbinstances and creating dynamoDB
	// instance

	// @Bean
	// public AmazonDynamoDB amazonDynamoDB() {
	// AmazonDynamoDBClient amazonDynamoDB = new AmazonDynamoDBClient(new
	// BasicAWSCredentials(amazonAWSAccessKey, amazonAWSSecretKey));
	// DynamoDB dynamoDB = new DynamoDB(amazonDynamoDB);
	// amazonDynamoDB.configureRegion(Regions.fromName(dbRegion));
	// return amazonDynamoDB;
	// }
	// AmazonDynamoDB amazonDynamoDBclient;

	@Bean
	public AmazonDynamoDB amazonDynamoDB(AWSCredentials awsCredentials) {
		AmazonDynamoDB amazonDynamoDBclient = new AmazonDynamoDBClient(awsCredentials);
		// ((AmazonWebServiceClient)
		// amazonDynamoDBclient).setSignerRegionOverride(Regions.fromName(dbRegion).getName());
		// amazonDynamoDBclient.configureRegion(Regions.fromName(dbRegion));
		Region usWest2 = Region.getRegion(Regions.US_WEST_2);
		amazonDynamoDBclient.setRegion(usWest2);
		return amazonDynamoDBclient;
	}

	@Bean
	public DynamoDB dynamodb(AmazonDynamoDB amazonDynamoDB) {
		DynamoDB dydb = new DynamoDB(amazonDynamoDB);
		return dydb;
	}

	@Bean
	public AWSCredentials awsCredentials() {
		return new BasicAWSCredentials(amazonAWSAccessKey, amazonAWSSecretKey);
	}

	@Bean
	public DynamoDBMapper dyanmap() {
		DynamoDBMapper dynam = new DynamoDBMapper(amazonDynamoDB(awsCredentials()));
		return dynam;
	}
}