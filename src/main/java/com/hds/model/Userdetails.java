package com.hds.model;

import java.util.List;

import org.json.JSONObject;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

@DynamoDBTable(tableName = "Userdetails")
public class Userdetails {

	private String Username;
	private String userid;
	private int policyid;
	private List<String> claimslist;
	private ContactDetails contactDetails;

	public Userdetails(String username, String userid, int policyid, List<String> claimslist,
			ContactDetails contactDetails) {
		super();
		Username = username;
		this.userid = userid;
		this.policyid = policyid;
		this.claimslist = claimslist;
		this.contactDetails = contactDetails;
	}

	public Userdetails() {
		super();

	}

	@DynamoDBAttribute(attributeName = "Username")
	public String getUsername() {
		return Username;
	}

	public void setUsername(String username) {
		Username = username;
	}

	@DynamoDBHashKey(attributeName = "userid")
	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	@DynamoDBAttribute(attributeName = "policyid")
	public int getPolicyid() {
		return policyid;
	}

	public void setPolicyid(int policyid) {
		this.policyid = policyid;
	}

	@DynamoDBAttribute(attributeName = "claimslist")
	public List<String> getClaimslist() {
		return claimslist;
	}

	public void setClaimslist(List<String> claimslist) {
		this.claimslist = claimslist;
	}

	@DynamoDBAttribute(attributeName = "contactDetails")
	public ContactDetails getContactDetails() {
		return contactDetails;
	}

	public void setContactDetails(ContactDetails contactDetails) {
		this.contactDetails = contactDetails;
	}

	@Override
	public String toString() {
		return "Userdetails [Username=" + Username + ", userid=" + userid + ", policyid=" + policyid + ", claimslist="
				+ claimslist + ", contactDetails=" + contactDetails + "]";
	}

}
