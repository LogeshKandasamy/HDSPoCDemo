package com.hds.model;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;

@DynamoDBDocument
public class ContactDetails {

	private long mobileno;
	private String address;
	private String mailid;

	public ContactDetails(long mobileno, String address, String mailid) {
		super();
		this.mobileno = mobileno;
		this.address = address;
		this.mailid = mailid;
	}

	public ContactDetails() {
		super();
		// TODO Auto-generated constructor stub
	}

	public long getMobileno() {
		return mobileno;
	}

	public void setMobileno(long mobileno) {
		this.mobileno = mobileno;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getMailid() {
		return mailid;
	}

	public void setMailid(String mailid) {
		this.mailid = mailid;
	}

	@Override
	public String toString() {
		return "ContactDetails [mobileno=" + mobileno + ", address=" + address + ", mailid=" + mailid + "]";
	}

}
