package com.hds.controller;

import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.hds.service.*;
import com.hds.model.Userdetails;

@RestController
public class DynamoDbController {

	@Autowired(required = true)
	UserService userService;

	@RequestMapping(value = "/create")
	public ResponseEntity<?> createDynamoTable() {
		System.out.println("table request creation");
		userService.createDynamoTable();

		return new ResponseEntity<>(HttpStatus.CREATED);
	}

	@RequestMapping(value = "/adduser")
	public ResponseEntity<?> adduser() throws JSONException {
		System.out.println("inserting userdetails");
		userService.addUser();
		return new ResponseEntity<Userdetails>(HttpStatus.OK);
	}

	@RequestMapping(value = "/availableuser/{userid}", method = RequestMethod.GET)
	@ResponseBody
	public Userdetails getUserById(@PathVariable("userid") String userid) {
		System.out.println("Getting User with User Id : " + userid);
		Userdetails user = userService.get(userid);
		return user;

	}

	@RequestMapping(value = "/updatecustomer/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> updateUser(@PathVariable("id") String id) {
		System.out.println("Updating User address  with id {}" + id);

		Userdetails User = userService.updateUserInfo(id);
		return new ResponseEntity<Userdetails>(User, HttpStatus.OK);
	}

}
